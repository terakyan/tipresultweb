import React, { Component } from 'react';
import slider1 from './../assets/images/slider-1.jpg';
import banner from './../assets/images/banner.jpg';
import appleIco from './../assets/images/app-1.png';
import androidIco from './../assets/images/app-2.png';
import OwlCarousel from 'react-owl-carousel';

class SliderBanner extends Component {
  render() {
    return (
        <section className="slider-banner">
            <div className="container">
                <div className="row">
                    <div className="col-md-7 col-12">
                        <div className="slider">
                        <OwlCarousel className="owl-theme mainslider" items={1} dots={false} nav={true} autoplay={true} navText={['<','>']}>
                                <div className="item">
                                    <a href="javascript:void(0);"><img src={slider1} alt="sl" /></a>
                                </div>
                                <div className="item">
                                    <a href="javascript:void(0);"><img src={slider1} alt="sl" /></a>
                                </div>
                                <div className="item">
                                    <a href="javascript:void(0);"><img src={slider1} alt="sl" /></a>
                                </div>
                        </OwlCarousel>
                        </div>
                    </div>
                    <div className="col-md-5 col-12">
                        <div className="banner ">
                            <img src={banner} alt="banner" />
                                <div className="banner-ads d-flex flex-column">
                                    <a href="javascript:void(0);"><img src={appleIco} alt="b1" /></a>
                                    <a href="javascript:void(0);"><img src={androidIco} alt="b2" /></a>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
  }
}

export default SliderBanner;
