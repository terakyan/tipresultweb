import React, {Component} from 'react';
import PropTypes from 'prop-types';

class UpcomingGamesSportItem extends Component {
    constructor(props) {
        super(props);
    }
    checkActiveItem (i){
        if(this.props.isActive === i){
            return 'active';
        }
    }
    render() {
        return (
            <li className="nav-item">
                <a className={"nav-link " + (this.props.isActive?"active":"") } data-value={this.props.id} onClick={() => {this.props.handlerActiveItem(this.props.id)} } data-toggle="tab" href="javascript:void(0)">
                    <span className="icon">
                        <img src={this.props.imagePath} width={10} />
                    </span>
                    <span className="title">{this.props.name}</span>
                </a>
            </li>
        );
    }
}

UpcomingGamesSportItem.prototypes = {
    name: PropTypes.string.IsRequired,
    imagePath: PropTypes.string.IsRequired,
};

export default UpcomingGamesSportItem;
