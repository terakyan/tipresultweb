import React from 'react';
import PropTypes from 'prop-types';
import cup from './../assets/images/cup-icon.png';
import cskabg from './../assets/images/cska-bg.jpg';
import cska from './../assets/images/CSKA-icon.png';

class FeaturedGameItem extends React.Component
{
    render()
    {
        return  <div className="col-md-6 col-xs-12 col-lg-4">
                        <div className="single-game d-flex">
                            <div className="cup d-flex flex-column justify-content-center align-items-center">
                                <img src={cup} alt="cup" />
                                    <span className="month">{this.props.gameStartDate}</span>
                                    <time className="time">{this.props.gameStartTime}</time>
                            </div>
                            <div className="team">
                                <img className="bg-team" src={cskabg} alt="cska" />
                                    <div className="all-team">
                                        <div className="team1 d-flex justify-content-between align-items-center">
                                            <img className="logo" src={this.props.homeTeamLogo} alt="cska" />
                                                <span className="title">{this.props.homeTeamName}</span>
                                        </div>
                                        <div className="team1 tm2 d-flex justify-content-between align-items-center">
                                            <img className="logo" src={this.props.awayTeamLogo} alt="cska" />
                                                <span className="title">{this.props.awayTeamName}</span>
                                        </div>
                                        <div className="tip-now">
                                            <a href="javascript:void(0)">Tip now</a>
                                        </div>
                                    </div>
                            </div>
                     </div>
                </div>
    }
}

FeaturedGameItem.prototypes = {
    cupLogo: PropTypes.string,
    competitionLogo: PropTypes.string,
    gameStartDate: PropTypes.string.IsRequired,
    gameStartTime: PropTypes.string.IsRequired,
    homeTeamLogo: PropTypes.string.IsRequired,
    awayTeamLogo: PropTypes.string.IsRequired,
    homeTeamName: PropTypes.string.IsRequired,
    awayTeamName: PropTypes.string.IsRequired
}

export default FeaturedGameItem;