import React, {Component} from 'react';
import PropTypes from 'prop-types';

class UpcomingGameItem extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return <tr className="d-flex">
                <td className="col-sm-1 text-center">{this.props.gameStartTime}</td>
                <td className="text-right col-sm-3">{this.props.homeTeamName}</td>
                <td className="col-sm-1 text-center vs">VS</td>
                <td className="col-sm-3">{this.props.awayTeamName}</td>
                <td className="tip-now col-sm-1 text-center">Tip now</td>
                <td className="tip-now col-sm-1 text-center">Tip now</td>
                <td className="tip-now col-sm-1 text-center active">Tip now</td>
                <td className="col-sm-1 text-center">+14</td>
            </tr>
    }
}

UpcomingGameItem.prototypes = {
    gameStartTime: PropTypes.string.IsRequired,
    homeTeamName: PropTypes.string.IsRequired,
    awayTeamName: PropTypes.string.IsRequired
};

export default UpcomingGameItem;
