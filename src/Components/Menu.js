import React, { Component } from 'react';

class Menu extends Component {
  render() {
    return (
        <ul className="navbar-nav mr-auto">
            <li className="nav-item active">
                <a className="nav-link" href="/">All Tips</a>
            </li>
            <li className="nav-item">
                <a className="nav-link" href="/">Express</a>
            </li>
            <li className="nav-item">
                <a className="nav-link" href="/">Football</a>
            </li>
            <li className="nav-item">
                <a className="nav-link" href="/">Basketball</a>
            </li>
            <li className="nav-item">
                <a className="nav-link" href="/">Other</a>
            </li>
        </ul>
    );
  }
}

export default Menu;
