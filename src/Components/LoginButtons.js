import React, { Component } from 'react';

class LoginButtons extends Component {
  render() {
    return (
        <div className="d-flex sign-reg ">
            <div className="sign"><a href="/">SIGN IN</a></div>
            <div className="reg active"><a href="/">REGISTER</a></div>
        </div>
    );
  }
}

export default LoginButtons;
