import React, { Component } from 'react';
import logo from './../assets/images/tipresult-logo.png'

class Logo extends Component {
  render() {
    return (
        <a className="navbar-brand" href="/">
            <img src={logo} alt="logo"/>
        </a>
    );
  }
}

export default Logo;
