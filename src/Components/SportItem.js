import React from 'react';
import PropTypes from 'prop-types';

class SportItem extends React.Component
{
    render()
    {
        return <div className="item">
                    <div className="single-tip">
                        <div className="top">
                            <img src={this.props.imagePath} alt={this.props.name} />
                                <span className="title">{this.props.name}</span>
                        </div>
                        <div className="bottom">
                            Your tip
                        </div>
                    </div>
                </div>
    }
}

SportItem.prototypes = {
  name:PropTypes.string.IsRequired,
  imagePath: PropTypes.string.IsRequired,
};

export default SportItem;