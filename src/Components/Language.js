import React, { Component } from 'react';

class Language extends Component {
  render() {
    return (
        <ul>
            <li className="nav-item dropdown">
                <a className="nav-link dropdown-toggle" href="/" id="navbarDropdown" role="button"
                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    ENG
                </a>
                <div className="dropdown-menu langs" aria-labelledby="navbarDropdown">
                    <a className="dropdown-item" href="/">RUS</a>
                    <a className="dropdown-item" href="/">ARG</a>
                </div>
            </li>
        </ul>
    );
  }
}

export default Language;
