import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { combineReducers } from 'redux';
import thunk from "redux-thunk";
import  sportReducer  from "../src/tipresultcore/reduces/SportReducer";
import  gameReducer  from "../src/tipresultcore/reduces/GameReducer"


const tipResultApp = combineReducers({
    sportReducer,
    gameReducer
})

let store = createStore(tipResultApp, applyMiddleware(thunk))

ReactDOM.render(<Provider store={store}><App /></Provider>, document.getElementById('root'));
registerServiceWorker();
