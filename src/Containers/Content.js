import React, { Component } from 'react';
import SliderBanner from '../Components/SliderBanner';
import Sports from '../Containers/Sports';
import FeaturedGames from '../Containers/FeaturedGames';
import UpcomingGames from '../Containers/UpcomingGames';

class Content extends Component {
  render() {
    return (
        <div>
            <SliderBanner></SliderBanner>
            <Sports></Sports>
            <FeaturedGames></FeaturedGames>
            <UpcomingGames></UpcomingGames>
        </div>
    );
  }
}

export default Content;
