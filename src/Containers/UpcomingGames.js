import React, {Component} from 'react';
import cup from './../assets/images/cup-icon.png';
import cskabg from './../assets/images/cska-bg.jpg';
import cska from './../assets/images/CSKA-icon.png';
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import {getSportsData} from '../tipresultcore/api/SportApi.js'
import {getUpcomingGamesBySportData} from '../tipresultcore/api/GameApi.js'
import {selectedSportChanged} from '../tipresultcore/actions/SportActions';
import UpcomingGamesSportItem from '../Components/UpcomingGamesSportItem.js';
import UpcomingGameItem from '../Components/UpcomingGameItem.js';

const mapStateToProps = (state) => {
    return {
        sports: state.sportReducer.sports,
        selected_sport: state.sportReducer.selected_sport,
        games: state.gameReducer.upgames,
        isLoaded: state.sportReducer.isLoaded,
        isLoadedGames: state.gameReducer.isLoadedGames,
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        getSportsData,
        getUpcomingGamesBySportData,
        selectedSportChanged
    }, dispatch);
}

class UpcomingGames extends Component {
    constructor(props) {
        super(props);
        this.activeItem = this.activeItem.bind(this);
    };

    activeItem(id){
       this.props.selectedSportChanged(id);
    }

    sportResult() {
        return this.props.sports.map((s, index) =>
            <UpcomingGamesSportItem key={index} isActive={s.id === this.props.selected_sport} handlerActiveItem={this.activeItem} id={s.id} name={s.name} imagePath={s.icon}/>
        );
    }
    sportList() {
        if (this.props.isLoaded) {
            return <ul className="nav nav-tabs" role="tablist">
                {this.sportResult()}
            </ul>;
        }
    }

    gameResult() {
        if(this.props.isLoadedGames){
            return this.props.games.map((g,index) =>
            {
                let date = new Date(g.start_date);
                return <UpcomingGameItem  key={index} id={g.id} gameStartTime={date.toLocaleTimeString()}
                                           homeTeamName={g.team1.name} awayTeamName={g.team2.name}/>
            } );
        }
    }

    gamesList() {
        if (this.props.isLoaded) {
            return this.gameResult();
        }
    }

    render() {
        return (
            <section className="upcoming-events">
                <div className="container">
                    <div className="row">
                        <div className="col-12">
                            <h3>UPCOMING EVENTS</h3>
                            <div className="">
                                {this.sportList()}
                                <div className="tab-content">
                                    <div id="football" className="container tab-pane active">
                                        <div className="table-responsive">
                                            <table className="table table-hover table-striped table-dark">
                                                <thead>
                                                <tr className="d-flex">
                                                    <th scope="col" className="col-1 text-center">TIME</th>
                                                    <th scope="col" className="text-right col-3">TEAM 1</th>
                                                    <th scope="col" className="col-1"></th>
                                                    <th scope="col" className="col-3">TEAM 2</th>
                                                    <th scope="col" className="col-1 text-center">1</th>
                                                    <th scope="col" className="col-1 text-center">X</th>
                                                    <th scope="col" className="col-1 text-center">2</th>
                                                    <th scope="col" className="col-1 text-center"></th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                    {this.gamesList()}
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        );
    }

    componentDidMount()
    {
        this.props.getSportsData();
    }

    componentWillReceiveProps(nextProps){
        if(this.props.selected_sport !== nextProps.selected_sport){
            this.props.getUpcomingGamesBySportData(nextProps.selected_sport);
        }
    }

}


export default connect(mapStateToProps, mapDispatchToProps)(UpcomingGames);
