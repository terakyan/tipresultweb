import React from 'react';
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import {getFeaturedGamesData} from '../tipresultcore/api/GameApi.js'
import FeaturedGameItem from '../Components/FeaturedGameItem'



const mapStateToProps = (state) => {
    return {
        featuredGames: state.gameReducer.games
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({ getFeaturedGamesData
    }, dispatch);
}


class FeaturedGames extends React.Component {
  
    constructor(props)
    {
        super(props);         
    };

    featuredGamesList() {
        return this.props.featuredGames.map((g) => 
        {
            var date = new Date(g.start_date);
            return <FeaturedGameItem    key={g.id} gameStartDate={date.toLocaleDateString()} gameStartTime={date.toLocaleTimeString()}
                                        homeTeamLogo={g.team1.icon} awayTeamLogo={g.team2.icon}
                                        homeTeamName={g.team1.name} awayTeamName={g.team2.name}/>
                                    } );
    }

    render() {
    return (
        <section className="featured-games">
            <div className="container">
                <div className="row">
                    <div className="col-12 all-featured">
                        <h3>Featured Games</h3>
                        <div className="row">
                        {this.featuredGamesList()}
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
  }

  componentDidMount()
  {
    this.props.getFeaturedGamesData();
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(FeaturedGames);
