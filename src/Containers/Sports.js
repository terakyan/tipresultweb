import React, { Component } from 'react';
import soccer from './../assets/images/soccer-ball-icon.png';
import tennis from './../assets/images/tennis-ball-icon.png';
import volleyball from './../assets/images/volleyball-icon.png';
import basketball from './../assets/images/basketball-icon.png';
import boxing from './../assets/images/boxing-icon.png';
import SportItem from '../Components/SportItem.js';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { getSportsData } from '../tipresultcore/api/SportApi.js'
import OwlCarousel from 'react-owl-carousel';

const mapStateToProps = (state) => {
    return {
        sports: state.sportReducer.sports,
        isLoaded: state.sportReducer.isLoaded
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({ getSportsData
    }, dispatch);
}

class Sports extends Component {

    constructor(props)
    {
        super(props);         
    };

    sportList() {
        if(this.props.isLoaded)
        {
            return <OwlCarousel  className="owl-theme"  startPosition={0}  loop margin={5} dots={false}>
                     {this.props.sports.map((s, index) => <SportItem key={index} name={s.name} imagePath={s.icon}/> )};
                   </OwlCarousel>;
        }
    }

  render() {  

    return (
        <section className="your-tip">
            <div className="container">
                <div className="row">
                    <div className="slider col-12">
                           {this.sportList()}
                    </div>
                </div>
            </div>
        </section>
    );
  }

  componentDidMount()
  {
    this.props.getSportsData();
  }
}

// Sports.prototypes={
//     sports: PropTypes.array.IsRequired
// }

export default connect(mapStateToProps, mapDispatchToProps)(Sports);

