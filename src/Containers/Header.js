import React, { Component } from 'react';
import Menu from '../Components/Menu';
import Language from '../Components/Language';
import LoginButtons from '../Components/LoginButtons';
import Logo from '../Components/Logo'

class Header extends Component {
  render() {
    return (
        <header>
            <div className="container-fluid">
                <div className="row">
                    <nav className="navbar navbar-expand-lg navbar-light">
                        <Logo/>

                        <button className="navbar-toggler" type="button" data-toggle="collapse"
                                data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                                aria-expanded="false" aria-label="Toggle navigation">
                            <span className="navbar-toggler-icon"></span>
                        </button>
                        <div className="collapse navbar-collapse" id="navbarSupportedContent">
                            <Menu />

                            <div className="form-inline my-2 my-lg-0 right">
                                <Language />
                                <LoginButtons />
                            </div>
                        </div>
                    </nav>
                </div>
            </div>
        </header>
    );
  }
}

export default Header;
