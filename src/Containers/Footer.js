import React, { Component } from 'react';
import tipresult from './../assets/images/tipresult-logo-min.png';
import hook from './../assets/images/hook-logo.png';


class Footer extends Component {
  render() {
    return (
        <footer>
            <div className="container">
                <div className="row">
                    <div className="d-flex justify-content-center align-items-center flex-column all-footer">
                        <div className="social">
                            <a href="javascript:void(0);"><i className="fab fa-instagram"></i></a>
                            <a href="javascript:void(0);"><i className="fab fa-facebook-square"></i></a>
                            <a href="javascript:void(0);"><i className="fab fa-vk"></i></a>
                        </div>
                        <div className="logo d-inline-flex">
                            <a href="javascript:void(0);">
                                <img src={tipresult} alt="tipresult" className="align-self-center" />
                            </a>
                            <div className="divide"></div>
                            <a href="javascript:void(0);">
                                <img src={hook} alt="hook" className="align-self-center" />
                            </a>
                        </div>
                        <div className="copy">
                            Powered by Hook LLC | All rights reserved 2018 <span>&#174</span>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    );
  }
}

export default Footer;
